# Laundry

## Tools
- basket
- drying rack
- hanger
- clips, clips rack

## Consumable
- washing powder


# Kitchen

## Cultery
- bowl, plate
- rack
- fork, knife, spoon, chopstick
- dinning mats

## Utensil
- knife
- spoon
- grater
- gloves
- colander
- spectula
- chopping board
- baking tray
- balance
- peeler
- rubbish bin

## Consumable
- soap
- sponge
- paper towel
- plastic wrap
- aluminum film
- rubbish bags
- water filter

## Cooking ware
- sauce pan
- frying pan
- rice cooker
- pressure cooker
- pot
- kettle

# Bathroom

## Consumable
- body soap
- shampoo
- conditioner
- toilet paper
- toothbrush, paste
- toilet brush/ jel
- soap rack
- bath towel
- hooks
- shower filter


# Bedroom

## Bed
- pillow
- duvet
- covers of all
- power extension
- glass cleaning agent
- slippers